<?php
if(strpos($_SERVER['REQUEST_URI'], 'localizacao.php') !== false){
	$url02 = '/localizacao';
	header('location: ' . $url02);exit;
}
?>
<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Localização | Gallotti Empreendimentos e Serviços | Locação de Veículos | Locação de Mão de Obra</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="A Gallotti tem serviços como Locação de Veículos, Locação de veículos leves, Locação de Mão de Obra, Locação de Veículos Pesados, limpeza e conservação predial, transporte de cargas rodoviárias, construção civil, entre outros." />
<meta name = "Author" content = "GOWEB Tecnologia - http://www.gowebtecnologia.com.br">
<meta name="publisher" content="GOWEB Tecnologia - http://www.gowebtecnologia.com.br" />
<meta name="robots" content="index, follow">
<link rel="shortcut icon" href="favicon.ico">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/animate.css"><link rel="stylesheet" href="css/flexslider.css"><link rel="stylesheet" href="css/icomoon.css"><link rel="stylesheet" href="css/magnific-popup.css"><link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/gallotti.css">
<script src="js/modernizr-2.6.2.min.js"></script>
<!--[if lt IE 9]><script src="js/respond.min.js"></script><![endif]-->
</head>
<body>
<?php include 'incs/inc_google.php'; ?>
<div class="gallotti_carregando"></div>
<div id="gallotti_page">
<?php include 'incs/inc_menu.php'; ?>
<?php include 'incs/inc_topo_localizacao.php'; ?>
<?php include 'incs/inc_localizacao.php'; ?>
<?php include 'incs/inc_rodape.php'; ?>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>

