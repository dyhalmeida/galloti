<?php include_once 'inc_fns.php';
if(strpos($_SERVER['REQUEST_URI'], 'trabalhe-conosco.php') !== false){
	$url02 = '/trabalhe-conosco';
	header('location: ' . $url02);exit;
}
?>
<?php
session_start();
if($_SESSION['nome_t'] == ''){
	$nome_t = 'Nome';
	}else{$nome_t = $_SESSION['nome_t'];}
if($_SESSION['email_t'] == ''){
	$email_t = 'E-mail';
	}else{$email_t = $_SESSION['email_t'];}	
if($_SESSION['telefone_t'] == ''){
	$telefone_t = 'Telefone';
	}else{$telefone_t = $_SESSION['telefone_t'];}
	
	
	
	if($_SESSION['telefone_t2'] == ''){
	$telefone_t2 = 'Telefone 2';
	}else{$telefone_t2 = $_SESSION['telefone_t2'];}
	
	
	
	if($_SESSION['mensagem_t'] == ''){
	$mensagem_t = 'Mensagem';
	}else{$mensagem_t = $_SESSION['mensagem_t'];}

if($_SESSION['data_nascimento'] == ''){
	$data_nascimento = 'Nascimento';
	}else{$data_nascimento = $_SESSION['data_nascimento'];}

if($_SESSION['cargo'] == ''){
	$cargo = 'Cargo';
	}else{$cargo = $_SESSION['cargo'];}

if($_SESSION['cidade'] == ''){
	$cidade = 'Cidade';
	}else{$cidade = $_SESSION['cidade'];}

$sexo = $_SESSION['sexo'];
$estadoescolhido = $_SESSION['estado'];



if(!empty($_SESSION['msg_erro_email'])){$msg_erro_email=$_SESSION['msg_erro_email'];}
if(!empty($_SESSION['msg_erro_nascimento'])){$msg_erro_nascimento=$_SESSION['msg_erro_nascimento'];}
if(!empty($_SESSION['msg_erro_telefone'])){$msg_erro_telefone=$_SESSION['msg_erro_telefone'];}
if(!empty($_SESSION['msg_erro_telefone2'])){$msg_erro_telefone2=$_SESSION['msg_erro_telefone2'];}
if(!empty($_SESSION['erro'])){$erro=$_SESSION['erro'];}
if(!empty($_SESSION['mensagem_erro'])){$mensagem_erro=$_SESSION['mensagem_erro'];}
if(!empty($_SESSION['erro'])){$erro=$_SESSION['erro'];}




?>
<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Trabalhe Conosco | Gallotti Empreendimentos e Serviços | Locação de Veículos | Locação de Mão de Obra</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="A Gallotti tem serviços como Locação de Veículos, Locação de veículos leves, Locação de Mão de Obra, Locação de Veículos Pesados, limpeza e conservação predial, transporte de cargas rodoviárias, construção civil, entre outros." />
<meta name = "Author" content = "GOWEB Tecnologia - http://www.gowebtecnologia.com.br">
<meta name="publisher" content="GOWEB Tecnologia - http://www.gowebtecnologia.com.br" />
<meta name="robots" content="index, follow">
<link rel="shortcut icon" href="favicon.ico">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/animate.css"><link rel="stylesheet" href="css/flexslider.css"><link rel="stylesheet" href="css/icomoon.css"><link rel="stylesheet" href="css/magnific-popup.css"><link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/gallotti.css">
<link rel="stylesheet" type="text/css" href="css/component.css" />
<script src="js/modernizr-2.6.2.min.js"></script>
<!--[if lt IE 9]><script src="js/respond.min.js"></script><![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/jquery.cascade-select.js" type="text/javascript"></script>
</head>
<body>
<?php include 'incs/inc_google.php'; ?>
<div class="gallotti_carregando"></div>
<div id="gallotti_page">
<?php include 'incs/inc_menu.php'; ?>
<?php include 'incs/inc_topo_trabalheconosco.php'; ?>
<?php include 'incs/inc_trabalheconosco.php'; ?>
<?php include 'incs/inc_rodape.php'; ?>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
<script src="js/mascara.js"></script>
<script src="js/custom-file-input.js"></script>
</body>
</html>
<?php 
unset($_SESSION['erro']);
unset($_SESSION['mensagem_erro']);
unset($_SESSION['msg_erro_email']);
unset($_SESSION['msg_erro_telefone']);
unset($_SESSION['msg_erro_telefone2']);
unset($_SESSION['msg_erro_nascimento']);
?>

