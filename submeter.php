<?php
session_start();
header('Content-Type: text/html; charset=UTF-8');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;

// Alias the League Google OAuth2 provider class
use League\OAuth2\Client\Provider\Google;

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';
require 'PHPMailer-master/src/OAuth.php';

// Load Composer's autoloader
require 'PHPMailer-master/vendor/autoload.php';

$_SESSION['nome'] = $_POST['nome'];
$_SESSION['email'] = $_POST['email'];
$_SESSION['telefone'] = $_POST['telefone'];
$_SESSION['mensagem'] = $_POST['mensagem'];


if(isset($_POST['submit']))
{
	
$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$mensagem = $_POST['mensagem'];
$ano = $_POST['ano'];


//------------Validação-------------
if(empty($nome)||empty($email)||empty($telefone)||empty($mensagem)|| $nome == "Nome" || $email == "E-mail" || $telefone == "Telefone" || $mensagem == "Mensagem" ){
$_SESSION['errors'] = "Todos os campos são de preenchimento obrigatório. ";	
echo "<script>window.location.href='contato.php';</script>";exit;

}
function VerifyEmailAddress($email) {
    list($User, $Domain) = explode("@", $email);
    $Result = checkdnsrr($Domain, 'MX');
	return($Result);
}
if(VerifyEmailAddress($email) != 1) {
$_SESSION['errors'] = "Por favor digite um e-mail válido. ";	
echo "<script>window.location.href='contato.php';</script>";exit;
	
}
$contar_telefone = strlen($telefone);
if($contar_telefone < 14){
	$_SESSION['errors'] = "Por favor digite o telefone corretamente. ";	
echo "<script>window.location.href='contato.php';</script>";exit;

	}
//Visitante recebe e-mail daqui:
//Create a new PHPMailer instance
$mail = new PHPMailer();;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// SMTP::DEBUG_OFF = off (for production use)
// SMTP::DEBUG_CLIENT = client messages
// SMTP::DEBUG_SERVER = client and server messages
$mail->SMTPDebug = 0;
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption mechanism to use - STARTTLS or SMTPS
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Set AuthType to use XOAUTH2
$mail->AuthType = 'XOAUTH2';
//Fill in authentication details here
//Either the gmail account owner, or the user that gave consent
//$email = 'someone@gmail.com';
//$clientId = 'RANDOMCHARS-----duv1n2.apps.googleusercontent.com';
//$clientSecret = 'RANDOMCHARS-----lGyjPcRtvP';
//Obtained by configuring and running get_oauth_token.php
//after setting up an app in Google Developer Console.
//$refreshToken = 'RANDOMCHARS-----DWxgOvPT003r-yFUV49TQYag7_Aod7y0';
//Create a new OAuth2 provider instance

$mail->oauthUserEmail="sistemadeenviodeemails2@gmail.com" ; 
$mail->oauthClientId="670564533872-vsshf287uvobr350pq5q6ceiqmk4o735.apps.googleusercontent.com" ; 
$mail->oauthClientSecret="f1CvNhg9d0H1uKAxFmgD7YZ8" ; 
$mail->oauthRefreshToken="1//0ft3SD-4WL5EZCgYIARAAGA8SNwF-L9IrIm4KWgPLWeMVeoK-X6xVSVAThurm5DXiOwN9x45GusaIN9nZGgoOBJ0XvFGqXBb7BW4" ;

$emailenvio = 'sistemadeenviodeemails2@gmail.com';
$clientId = '670564533872-vsshf287uvobr350pq5q6ceiqmk4o735.apps.googleusercontent.com';
$clientSecret = 'f1CvNhg9d0H1uKAxFmgD7YZ8';
$refreshToken = '1//0ft3SD-4WL5EZCgYIARAAGA8SNwF-L9IrIm4KWgPLWeMVeoK-X6xVSVAThurm5DXiOwN9x45GusaIN9nZGgoOBJ0XvFGqXBb7BW4';

$provider = new Google(
    [
        'clientId' => $clientId,
        'clientSecret' => $clientSecret,
    ]
);
//Pass the OAuth provider instance to PHPMailer
$mail->setOAuth(
    new OAuth(
        [
            'provider' => $provider,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'refreshToken' => $refreshToken,
            'userName' => $emailenvio,
        ]
    )
);


//$email = "goweb@gowebtecnologia.com.br";
//$nome = "Goweb Tecnologia";
//Set who the message is to be sent from
//For gmail, this generally needs to be the same as the user you logged in as
$mail->setFrom($emailenvio, 'Gallotti Empreendimentos e Serviços');
//Set who the message is to be sent to
$mail->addAddress('goweb@gowebtecnologia.com.br', 'Goweb Tecnologia');
//$mail->addAddress('gallotti@gallotti.com.br', 'Gallotti Empreendimentos');
//Set the subject line
$mail->Subject = 'Contato Enviado | Gallotti Empreendimentos e Serviços';
$mail->AddReplyTo(''.$email.'', ''.$nome.'');
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->CharSet = PHPMailer::CHARSET_UTF8;
$mail->msgHTML(
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title>E-mail Enviado</title>
    <style type="text/css">  
    #outlook a {
      padding: 0;
    }
    body {
      width: 100% !important;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      margin: 0;
      padding: 0;
    }
    .ExternalClass {
      width: 100%;
    }
    .ExternalClass,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }
    .ExternalClass p {
      line-height: inherit;
    }
    #body-layout {
      margin: 0;
      padding: 0;
      width: 100% !important;
      line-height: 100% !important;
    }
    img {
      display: block;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    a img {
      border: none;
    }
    table td {
      border-collapse: collapse;
    }
    table {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    a {
      color: orange;
      outline: none;
    }
	
	.img {width:100%; height:auto; max-width:137px;}
    </style>
  </head>
  <body id="body-layout" style="background: #f1f1f1;">
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td align="center" valign="top" style="padding: 0 15px;background: #f1f1f1;">
          <table align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td height="15" style="height: 15px; line-height:15px;"></td>
            </tr>
            <tr>
              <td width="600" align="center" valign="top" style="border-radius: 4px; overflow: hidden; box-shadow: 3px 3px 6px 0 rgba(0,0,0,0.2);background: #fff;">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top" style="border-top-left-radius: 4px; border-top-right-radius: 4px; overflow: hidden; padding: 0 20px;background: #d12621;">
                      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top" style="font-family: Arial, sans-serif; font-size: 32px; mso-line-height-rule: exactly; line-height: 32px; font-weight: 400; letter-spacing: 1px;color: #ffffff;"><img src="http://www.gallotti.net.br/imgs/logo_email_claro.png" class="img" border="0" height="90" /></td>
                        </tr>
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top" style="padding: 0 20px;">
                      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr> 
                        <tr> 
                          <td align="left" valign="top" style="font-family: Arial, sans-serif; font-size: 15px; mso-line-height-rule: exactly; line-height: 22px; font-weight: 500;color: #302f35; text-align:center;">
                          Olá, Administrador. Há um novo e-mail de contato enviado por <strong style="color:#000;">'. $nome .'</strong>. Abaixo seguem os detalhes:                                   
                                               
                          
                          
                          
                          </td>  
                        </tr>
                       
                        <tr> 
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top">
                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td align="center" valign="top" style="background: #d1d5da;">
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    
                                    
                                    
                        <tr>
                        <td width="100" height="auto" valign="middle" bgcolor="#d12621"  style="color:#ffffff;border-bottom:1px solid #fff; "><div align="right" style="padding:10px; "><strong>Nome: </strong></div></td><td width="" bgcolor="#ffffff" style="border: solid 1px #d12621; background:#fff; padding:10px;">'. $nome .'</td>
                        </tr>
                         <tr>
                        <td width="100" valign="middle" bgcolor="#d12621"  style="color:#ffffff;border-bottom:1px solid #fff;"><div align="right" style="padding:10px; "><strong>E-mail: </strong></div></td><td width="" bgcolor="#ffffff" style="border: solid 1px #d12621; border-top:none; background:#fff; padding:10px;">'. $email .'</td>
                        </tr>
                         <tr>
                        <td width="100" valign="middle" bgcolor="#d12621"  style="color:#ffffff; border-bottom:1px solid #fff;"><div align="right" style="padding:10px;"><strong>Telefone: </strong></div></td><td width="" bgcolor="#ffffff" style="border: solid 1px #d12621; border-top:none; background:#fff; padding:10px;">'. $telefone .'</td>
                        </tr>
                         <tr>
                        <td width="100" valign="middle" bgcolor="#d12621"  style="color:#ffffff; border-bottom:1px solid #d12621;"><div align="right" style="padding:10px; "><strong>Mensagem: </strong></div></td><td width="" bgcolor="#ffffff" style="border: solid 1px #d12621; border-top:none; background:#fff; padding:10px;line-height:25px;">'. $mensagem .'</td>
                        </tr>
                        
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" style="background: #e4e6e9;">
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td height="2" style="height: 2px; line-height:2px;"></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-family: Arial, sans-serif; font-size: 12px; mso-line-height-rule: exactly; text-align:center; color: #302f35;">
                          	Gallotti Empreendimentos e Serviços | © '. $ano .' - Cópia não autorizada.
                          </td>
                        </tr>
                        
                      
                        <tr>
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="height: 20px; line-height:20px;"></td>
            </tr>
            <tr>
              <td width="600" align="center" valign="top">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top" style="font-family: Arial, sans-serif; font-size: 12px; mso-line-height-rule: exactly; line-height: 18px; font-weight: 400;color: #000;"></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="height: 20px; line-height:20px;"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>');


$mail->Send();

//Visitante recebe e-mail daqui:

//Create a new PHPMailer instance
$mail = new PHPMailer();;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// SMTP::DEBUG_OFF = off (for production use)
// SMTP::DEBUG_CLIENT = client messages
// SMTP::DEBUG_SERVER = client and server messages
$mail->SMTPDebug = 0;
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption mechanism to use - STARTTLS or SMTPS
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Set AuthType to use XOAUTH2
$mail->AuthType = 'XOAUTH2';
//Fill in authentication details here
//Either the gmail account owner, or the user that gave consent
//$email = 'someone@gmail.com';
//$clientId = 'RANDOMCHARS-----duv1n2.apps.googleusercontent.com';
//$clientSecret = 'RANDOMCHARS-----lGyjPcRtvP';
//Obtained by configuring and running get_oauth_token.php
//after setting up an app in Google Developer Console.
//$refreshToken = 'RANDOMCHARS-----DWxgOvPT003r-yFUV49TQYag7_Aod7y0';
//Create a new OAuth2 provider instance

$mail->oauthUserEmail="sistemadeenviodeemails2@gmail.com" ; 
$mail->oauthClientId="670564533872-vsshf287uvobr350pq5q6ceiqmk4o735.apps.googleusercontent.com" ; 
$mail->oauthClientSecret="f1CvNhg9d0H1uKAxFmgD7YZ8" ; 
$mail->oauthRefreshToken="1//0ft3SD-4WL5EZCgYIARAAGA8SNwF-L9IrIm4KWgPLWeMVeoK-X6xVSVAThurm5DXiOwN9x45GusaIN9nZGgoOBJ0XvFGqXBb7BW4" ;

$emailenvio = 'sistemadeenviodeemails2@gmail.com';
$clientId = '670564533872-vsshf287uvobr350pq5q6ceiqmk4o735.apps.googleusercontent.com';
$clientSecret = 'f1CvNhg9d0H1uKAxFmgD7YZ8';
$refreshToken = '1//0ft3SD-4WL5EZCgYIARAAGA8SNwF-L9IrIm4KWgPLWeMVeoK-X6xVSVAThurm5DXiOwN9x45GusaIN9nZGgoOBJ0XvFGqXBb7BW4';

$provider = new Google(
    [
        'clientId' => $clientId,
        'clientSecret' => $clientSecret,
    ]
);
//Pass the OAuth provider instance to PHPMailer
$mail->setOAuth(
    new OAuth(
        [
            'provider' => $provider,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'refreshToken' => $refreshToken,
            'userName' => $emailenvio,
        ]
    )
);


//Set who the message is to be sent from
//For gmail, this generally needs to be the same as the user you logged in as
$mail->setFrom($emailenvio, 'Gallotti Empreendimentos e Serviços');
//Set who the message is to be sent to
$mail->addAddress(''.$email.'', ''.$nome.'');
//Set the subject line
$mail->Subject = 'Resposta automática - '.$assunto;
$mail->AddReplyTo('no-reply@gallotti.net.br', 'Não Responder');
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->CharSet = PHPMailer::CHARSET_UTF8;
$mail->msgHTML(
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title>E-mail Enviado</title>
    <style type="text/css">  
    #outlook a {
      padding: 0;
    }
    body {
      width: 100% !important;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      margin: 0;
      padding: 0;
    }
    .ExternalClass {
      width: 100%;
    }
    .ExternalClass,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }
    .ExternalClass p {
      line-height: inherit;
    }
    #body-layout {
      margin: 0;
      padding: 0;
      width: 100% !important;
      line-height: 100% !important;
    }
    img {
      display: block;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    a img {
      border: none;
    }
    table td {
      border-collapse: collapse;
    }
    table {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    a {
      color: orange;
      outline: none;
    }
	
	.img {width:100%; height:auto; max-width:137px;}
    </style>
  </head>
  <body id="body-layout" style="background: #f1f1f1;">
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td align="center" valign="top" style="padding: 0 15px;background: #f1f1f1;">
          <table align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td height="15" style="height: 15px; line-height:15px;"></td>
            </tr>
            <tr>
              <td width="600" align="center" valign="top" style="border-radius: 4px; overflow: hidden; box-shadow: 3px 3px 6px 0 rgba(0,0,0,0.2);background: #fff;">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top" style="border-top-left-radius: 4px; border-top-right-radius: 4px; overflow: hidden; padding: 0 20px;background: #d12621;">
                      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top" style="font-family: Arial, sans-serif; font-size: 32px; mso-line-height-rule: exactly; line-height: 32px; font-weight: 400; letter-spacing: 1px;color: #ffffff;"><img src="http://www.gallotti.net.br/imgs/logo_email_claro.png" class="img" border="0" height="90" /></td>
                        </tr>
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top" style="padding: 0 20px;">
                      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr> 
                        <tr> 
                          <td align="left" valign="top" style="font-family: Arial, sans-serif; font-size: 15px; mso-line-height-rule: exactly; line-height: 22px; font-weight: 400;color: #302f35; text-align:center;"><br><br>Olá, <strong style="color:#d12621;"> '. $nome .'</strong>. Seu e-mail foi enviado com sucesso. Obrigado! <br><br> Responderemos o mais breve possível.<br><br>Se preferir, poderá também entrar em contato pelos telefones: (79) 3211-9778 | (79) 3211-2168<br><br><br></td> 
                        </tr>
                        <tr> 
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top">
                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td align="center" valign="top" style="background: #d1d5da;">
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td height="1" style="height: 1px; line-height:1px;"></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" style="background: #e4e6e9;">
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td height="2" style="height: 2px; line-height:2px;"></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-family: Arial, sans-serif; font-size: 12px; mso-line-height-rule: exactly; text-align:center; color: #302f35;">
                          	Gallotti Empreendimentos e Serviços&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; © '. $ano .'.
                          </td>
                        </tr>
                        
                      
                        <tr>
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="height: 20px; line-height:20px;"></td>
            </tr>
            <tr>
              <td width="600" align="center" valign="top">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top" style="font-family: Arial, sans-serif; font-size: 12px; mso-line-height-rule: exactly; line-height: 18px; font-weight: 400;color: #000;">Este e-mail foi gerado automaticamente, por favor não responda.</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="height: 20px; line-height:20px;"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>');


$mail->Send();

echo "<script>window.location.href='done.php';</script>";




}



?>