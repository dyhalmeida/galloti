<?php session_start();
include_once 'inc_fns.php';
//Import PHPMailer classes into the global namespace
header('Content-Type: text/html; charset=UTF-8');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;

// Alias the League Google OAuth2 provider class
use League\OAuth2\Client\Provider\Google;

require 'PHPMailer-master/src/Exception.php';
require 'PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/src/SMTP.php';
require 'PHPMailer-master/src/OAuth.php';

// Load Composer's autoloader
require 'PHPMailer-master/vendor/autoload.php';

$_SESSION['nome_t'] = $_POST['nome_t'];
$_SESSION['email_t'] = $_POST['email_t'];
$_SESSION['telefone_t'] = $_POST['telefone_t'];
$_SESSION['telefone_t2'] = $_POST['telefone_t2'];
$_SESSION['sexo'] = $_POST['sexo'];
$_SESSION['data_nascimento'] = $_POST['data_nascimento'];
$_SESSION['cargo'] = $_POST['cargo'];
$_SESSION['cidade'] = $_POST['cidade'];
$_SESSION['estado'] = $_POST['estado'];


if(isset($_POST['submit']))
{
$nome = $_POST['nome_t'];
$email = $_POST['email_t'];
$telefone = $_POST['telefone_t'];
$telefone2 = $_POST['telefone_t2'];
$mensagem = $_POST['mensagem_t'];
$sexo = $_POST['sexo'];
$nascimento = $_POST['data_nascimento'];
$cargo = $_POST['cargo'];
$cidade = $_POST['cidade'];
$estado = $_POST['estado'];
$curriculo = $_POST['curriculo'];
$ano = $_POST['ano'];


if
(empty($nome)
||empty($email)
||empty($telefone)
||empty($sexo)
||empty($nascimento)
||empty($cargo)
||empty($cidade)
||empty($estado)
|| $nome == "Nome" 
|| $email == "E-mail" 
|| $telefone == "Telefone" 
|| $sexo == "Sexo"
|| $nascimento == "Nascimento"
|| $cargo == "Cargo"
|| $cidade == "Cidade"
|| $estado == "Estado" 
)
{
$_SESSION['mensagem_erro'] = "Todos os campos, exceto Telefone 2, são de preenchimento obrigatório. ";	
echo "<script>window.location.href='trabalhe-conosco.php';</script>";exit;

}
$sql = "SELECT count(id) as total FROM usuarios WHERE email = '$email'";
	$qtdResult = $db->GetRow($sql);
	if($qtdResult['total'] > 0){
		$_SESSION['mensagem_erro'] = "Este e-mail já está cadastrado. Caso tenha esquecido a senha clique em fazer login no botão acima.";	
echo "<script>window.location.href='trabalhe-conosco.php';</script>";exit;
	}

function VerifyEmailAddress($email) {
    list($User, $Domain) = explode("@", $email);
    $Result = checkdnsrr($Domain, 'MX');
	return($Result);
}
if(VerifyEmailAddress($email) != 1) {
$_SESSION['msg_erro_email'] = "Por favor digite um e-mail válido. ";	
$erro = '1';
$_SESSION['erro'] = 1;

}

$contar_nascimento = strlen($nascimento);
if($contar_nascimento < 10 || $contar_nascimento > 10){
	$_SESSION['msg_erro_nascimento'] = "Por favor digite a data de nascimento corretamente no formato xx/xx/xxxx. ";	
$erro = '1';
$_SESSION['erro'] = 1;

	}


$contar_telefone = strlen($telefone);
if($contar_telefone < 14 || $contar_telefone > 15){
	$_SESSION['msg_erro_telefone'] = "Por favor digite o telefone corretamente. ";	
$erro = '1';
$_SESSION['erro'] = 1;

	}
	
	if(!empty($telefone2) && $telefone2 != "Telefone 2"){
	$contar_telefone2 = strlen($telefone2);
if($contar_telefone2 < 14 || $contar_telefone2 > 15){
	$_SESSION['msg_erro_telefone2'] = "Por favor digite o telefone corretamente. ";	
$erro = '1';
$_SESSION['erro'] = 1;

	}
	}
	
	
	if($erro == '1'){
echo "<script>window.location.href='trabalhe-conosco.php';</script>";exit;

}else{

function sanitize_title($title) {
    // substitui espaços por "-"
    $title = preg_replace('#\s+#', '-', $title);
    // faz a transliteração pra ASCII
    $title = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $title);
    // remove qualquer outra coisa inválida da url
    $title = preg_replace('#[^a-zA-Z0-9_-]+#', '', $title);
    return strtolower($title);
}


// bloco para o alias
	$jaexiste = false;
	$aliastmp = sanitize_title($nome);
	$alias = $aliastmp;
	$sql = "SELECT count(*) as total FROM usuarios WHERE alias = '$alias'";
	$qtdResult = $db->GetRow($sql);
	if($qtdResult['total'] > 0){
		$jaexiste = true;
	}
	$cont = 1;
	while($jaexiste){
		$alias = $aliastmp . '-' . ++$cont;
		$sql = "SELECT count(*) as total FROM usuarios WHERE alias = '$alias'";
		$qtdResult = $db->GetRow($sql);
		if($qtdResult['total'] > 0){
			$jaexiste = true;
		}else{
			$jaexiste = false;
		}
		
	}
	$curriculo['alias'] = $alias;
	//fim bloco para o alias
	// bloco para o login
	$jaexiste = false;
	$aliastmp = sanitize_title($nome);
	$alias = $aliastmp;
	$sql = "SELECT count(*) as total FROM usuarios WHERE login = '$alias'";
	$qtdResult = $db->GetRow($sql);
	if($qtdResult['total'] > 0){
		$jaexiste = true;
	}
	$cont = 1;
	while($jaexiste){
		$alias = $aliastmp . '-' . ++$cont;
		$sql = "SELECT count(*) as total FROM usuarios WHERE alias = '$alias'";
		$qtdResult = $db->GetRow($sql);
		if($qtdResult['total'] > 0){
			$jaexiste = true;
		}else{
			$jaexiste = false;
		}
		
	}
	$curriculo['alias'] = $alias;
	//fim bloco para o login


$upper = implode('', range('A', 'Z')); // ABCDEFGHIJKLMNOPQRSTUVWXYZ
$lower = implode('', range('a', 'z')); // abcdefghijklmnopqrstuvwxyzy
$nums = implode('', range(0, 9)); // 0123456789

$alphaNumeric = $upper.$lower.$nums; // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
$string = '';
$len = 8; // numero de chars
for($i = 0; $i < $len; $i++) {
    $string .= $alphaNumeric[rand(0, strlen($alphaNumeric) - 1)];
}
$senha = $string; // ex: q02TAq3



$curriculo = $_POST;
$curriculo['alias'] = $alias;
    $curriculo['senha'] = md5($senha);
	$curriculo['grupo_id'] = 1;
	$curriculo['perfil_id'] = 1;
	$curriculo['status_aprovacao'] = 1;
	$curriculo['nome'] = $curriculo['nome_t'];
	$curriculo['email'] = $curriculo['email_t'];
	$curriculo['cargo'] = $curriculo['cargo'];
	$curriculo['sexo'] = $curriculo['sexo'];
	$curriculo['telefone'] = $curriculo['telefone_t'];
	if(!empty($curriculo['telefone_t2']) && $curriculo['telefone_t2'] != 'Telefone 2'){$curriculo['telefone2'] = $curriculo['telefone_t2'];}else{$curriculo['telefone2']='';};
	$datanascimento = explode('/', $curriculo['data_nascimento']);
	$curriculo['aniversario'] = $datanascimento[2] . '-' . $datanascimento[1] . '-' . $datanascimento[0];
	$curriculo['anonascimento'] = $datanascimento[2];
	$inserir = $db->AutoExecute('usuarios',$curriculo,'INSERT');
	
if($inserir){
	$id = $db->Insert_ID();
	if(!empty($_FILES['curriculo'])){
		
		$arquivo = $_FILES['curriculo'];
		$caminho = SITE_PATH . '/sgs/trabalheconosco/uploads/curriculos/' . $id;
		if(!is_dir($caminho)){
			@mkdir($caminho);
		}
		if($arquivo['error']){
			$sql = 'DELETE FROM usuarios WHERE id = ' . $id;
			$excluir = $db->Execute($sql);
			$_SESSION['mensagem_erro'] = "Houve problema no envio do currículo. Por favor, tente novamente.1 ";	
echo "<script>window.location.href='trabalhe-conosco.php';</script>";exit;

		}
		$extensoespermitidas = array('doc','docx', 'pdf', 'DOC', 'DOCX');
		
		$nomeArr = explode('.', $_FILES['curriculo']['name']);
		$extensao = $nomeArr[count($nomeArr) - 1];
		$resultadoExt = in_array($extensao,$extensoespermitidas);
if($resultadoExt != 1){
	$_SESSION['mensagem_erro'] = "Só serão aceitos arquivos Doc, Docx e PDF";	
echo "<script>window.location.href='trabalhe-conosco.php';</script>";exit;

	}
	
		$nomeImg = $id . '_' . time() . '.' . $extensao;
		$imagemenviada = false;
		if(move_uploaded_file($arquivo['tmp_name'], $caminho . '/' . $nomeImg)){
			$curriculo['curriculo'] = '/trabalheconosco/uploads/curriculos/' . $id . '/' . $nomeImg;
			$imagemenviada = true;
		}
	}
	$inserir = $db->AutoExecute('usuarios',$curriculo,'UPDATE', 'id = ' . $id);
	if($inserir){
		if(!$imagemenviada and !empty($_FILES['curriculo']['name'])){
						
		}
	}else{
		$sql = 'DELETE FROM usuarios WHERE id = ' . $id;
		$excluir = $db->Execute($sql);
					$_SESSION['mensagem_erro'] = "Houve problema no envio do arquivo. Por favor, tente novamente.3 ";	
echo "<script>window.location.href='trabalhe-conosco.php';</script>";exit;

	}
}




//Visitante recebe e-mail daqui:
//Create a new PHPMailer instance
$mail = new PHPMailer();;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// SMTP::DEBUG_OFF = off (for production use)
// SMTP::DEBUG_CLIENT = client messages
// SMTP::DEBUG_SERVER = client and server messages
$mail->SMTPDebug = 0;
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption mechanism to use - STARTTLS or SMTPS
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Set AuthType to use XOAUTH2
$mail->AuthType = 'XOAUTH2';
//Fill in authentication details here
//Either the gmail account owner, or the user that gave consent
//$email = 'someone@gmail.com';
//$clientId = 'RANDOMCHARS-----duv1n2.apps.googleusercontent.com';
//$clientSecret = 'RANDOMCHARS-----lGyjPcRtvP';
//Obtained by configuring and running get_oauth_token.php
//after setting up an app in Google Developer Console.
//$refreshToken = 'RANDOMCHARS-----DWxgOvPT003r-yFUV49TQYag7_Aod7y0';
//Create a new OAuth2 provider instance

$mail->oauthUserEmail="sistemadeenviodeemails2@gmail.com" ; 
$mail->oauthClientId="670564533872-vsshf287uvobr350pq5q6ceiqmk4o735.apps.googleusercontent.com" ; 
$mail->oauthClientSecret="f1CvNhg9d0H1uKAxFmgD7YZ8" ; 
$mail->oauthRefreshToken="1//0ft3SD-4WL5EZCgYIARAAGA8SNwF-L9IrIm4KWgPLWeMVeoK-X6xVSVAThurm5DXiOwN9x45GusaIN9nZGgoOBJ0XvFGqXBb7BW4" ;

$emailenvio = 'sistemadeenviodeemails2@gmail.com';
$clientId = '670564533872-vsshf287uvobr350pq5q6ceiqmk4o735.apps.googleusercontent.com';
$clientSecret = 'f1CvNhg9d0H1uKAxFmgD7YZ8';
$refreshToken = '1//0ft3SD-4WL5EZCgYIARAAGA8SNwF-L9IrIm4KWgPLWeMVeoK-X6xVSVAThurm5DXiOwN9x45GusaIN9nZGgoOBJ0XvFGqXBb7BW4';

$provider = new Google(
    [
        'clientId' => $clientId,
        'clientSecret' => $clientSecret,
    ]
);
//Pass the OAuth provider instance to PHPMailer
$mail->setOAuth(
    new OAuth(
        [
            'provider' => $provider,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'refreshToken' => $refreshToken,
            'userName' => $emailenvio,
        ]
    )
);

//Set who the message is to be sent from
//For gmail, this generally needs to be the same as the user you logged in as
$mail->setFrom($emailenvio, 'Trabalhe Conosco Gallotti Empreendimentos');
//Set who the message is to be sent to
$mail->addAddress(''.$email.'', ''.$nome.'');
//Set the subject line
$mail->Subject = 'Resposta Automática - Trabalhe Conosco Enviado | Gallotti Empreendimentos e Serviços';
$mail->AddReplyTo('no-reply@gallotti.net.br', 'Não Responder');

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$mail->CharSet = PHPMailer::CHARSET_UTF8;
$mail->msgHTML('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <title>E-mail Enviado</title>
    <style type="text/css">  
    #outlook a {
      padding: 0;
    }
    body {
      width: 100% !important;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      margin: 0;
      padding: 0;
    }
    .ExternalClass {
      width: 100%;
    }
    .ExternalClass,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }
    .ExternalClass p {
      line-height: inherit;
    }
    #body-layout {
      margin: 0;
      padding: 0;
      width: 100% !important;
      line-height: 100% !important;
    }
    img {
      display: block;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    a img {
      border: none;
    }
    table td {
      border-collapse: collapse;
    }
    table {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    a {
      color: black;
      outline: none;
    }
	
	.img {width:100%; height:auto; max-width:137px;}
    </style>
  </head>
  <body id="body-layout" style="background: #f1f1f1;">
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td align="center" valign="top" style="padding: 0 15px;background: #f1f1f1;">
          <table align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
              <td height="15" style="height: 15px; line-height:15px;"></td>
            </tr>
            <tr>
              <td width="600" align="center" valign="top" style="border-radius: 4px; overflow: hidden; box-shadow: 3px 3px 6px 0 rgba(0,0,0,0.2);background: #fff;">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top" style="border-top-left-radius: 4px; border-top-right-radius: 4px; overflow: hidden; padding: 0 20px;background: #d12621;">
                      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top" style="font-family: Arial, sans-serif; font-size: 32px; mso-line-height-rule: exactly; line-height: 32px; font-weight: 400; letter-spacing: 1px;color: #ffffff;"><img src="http://www.gallotti.net.br/imgs/logo_email_claro.png" class="img" border="0" height="90" /></td>
                        </tr>
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top" style="padding: 0 20px;">
                      <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td height="30" style="height: 30px; line-height:30px;"></td>
                        </tr> 
                        <tr> 
                          <td align="left" valign="top" style="font-family: Arial, sans-serif; font-size: 15px; mso-line-height-rule: exactly; line-height: 22px; font-weight: 400;color: #302f35; text-align:center;"><br><br>Olá, <strong style="color:#d12621;"> '. $nome .'</strong>. Seu currículo foi cadastrado com sucesso. Obrigado! <br><br>Sempre que desejar atualizar seu currículo em nossa base acesse o site da Gallotti Empreendimentos e faça o login com seus dados:<br><br>Usuário: '.$email.'<br>Senha: '.$senha.'<br><br>Obs.: Para sua segurança solicitamos que altere a sua senha ao fazer login<br><br><a href="http://www.gallotti.net.br">www.gallotti.net.br</a> <br><a href="http://www.florafertil.com.br">www.florafertil.com.br</a><br><br><br></td> 
                        </tr>
                        <tr> 
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                        <tr>
                          <td align="center" valign="top">
                            <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                              <tr>
                                <td align="center" valign="top" style="background: #d1d5da;">
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td height="1" style="height: 1px; line-height:1px;"></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" style="background: #e4e6e9;">
                                  <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                      <td height="2" style="height: 2px; line-height:2px;"></td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" style="font-family: Arial, sans-serif; font-size: 12px; mso-line-height-rule: exactly; text-align:center; color: #302f35;">
                          	Gallotti Empreendimentos e Serviços&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; © '. $ano .'.
                          </td>
                        </tr>
                        
                      
                        <tr>
                          <td height="20" style="height: 20px; line-height:20px;"></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="height: 20px; line-height:20px;"></td>
            </tr>
            <tr>
              <td width="600" align="center" valign="top">
                <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td align="center" valign="top" style="font-family: Arial, sans-serif; font-size: 12px; mso-line-height-rule: exactly; line-height: 18px; font-weight: 400;color: #000;">Este e-mail foi gerado automaticamente, por favor não responda.</td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td height="20" style="height: 20px; line-height:20px;"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>');


$mail->Send();

echo "<script>window.location.href='done-trabalhe-conosco.php';</script>";

}
}
?>