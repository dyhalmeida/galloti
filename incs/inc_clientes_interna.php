<div id="gallotti_aempresa">
<div class="container">
<div class="row">
<div id="galeria" class="thumbs">
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><img src="imgs/clientes/aracaju.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><img src="imgs/clientes/cosil.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><img src="imgs/clientes/deso.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><img src="imgs/clientes/habitacional.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><img src="imgs/clientes/petrobras.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><img src="imgs/clientes/secretaria.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><img src="imgs/clientes/vale.png" alt="Locação de Veículos Extrapesados" class="img-responsive"></figure>
</div>
</div>
</div>
</div>
</div>
</div>