<section id="gallotti_contato">
<div class="container">
<div class="col-contato col-sm-6 col-xs-12 animate-box">
<div class="gallotti_rodape-contato">
<p>
Av. Dr. Edézio Vieira de Melo, 954<br>Bairro Suíssa. Aracaju - Sergipe<br><a href="tel:+7932119778">(79) 3211-9778</a> | <a href="tel:+7932112168">(79) 3211-2168</a><br><a href="mailto:gallotti@gallotti.net.br">gallotti@gallotti.net.br</a> 
</p>
</div>
</div>
<?php
$sexosiglas = array('masculino','feminino'); 
$sexos = array('Masculino','Feminino');  
?> 
<style>
.fazerlogin { background:#c72127; padding:15px 10px; color:#fff; text-align:center; transition: 0.3s}
.fazerlogin:hover {background:#e62930;}
.top-mar { float:left;margin-top:30px;}
</style>

<form action="submeter_trabalheconosco.php" method="post" class="animate-box" enctype="multipart/form-data">
<div class="col-md-12">
<div class="row">
<a href="https://trabalheconosco.gallotti.net.br" target="_blank"><div class="fazerlogin">Se já possui login clique aqui</div></a>
</div>
</div>

<p class="top-mar">Se não possui login, não se preocupe. Cadastre-se prenchendo o formulario abaixo. É rápido e você recebe os dados de login por e-mail. Por isso preencha corretamente seu e-mail.</p>

 <?php if(!empty($mensagem_erro)){echo "<div class='erro_atendimento send_error'>".nl2br($mensagem_erro)."</div>";}?>
 <?php if(isset($erro)){if($erro==1){echo "<div class='erro_atendimento send_error'>Houve erro! Verifique os campos corretamente e anexe seu currículo novamente.</div>";}}?>

 <input type="text" name="nome_t" class="input_contato"  maxlength="80" value="<?php echo htmlentities($nome_t) ?>"  onFocus="if (this.value == 'Nome') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Nome' };"/>
<input type="text" name="email_t" class="input_contato"   maxlength="100" value="<?php echo htmlentities($email_t) ?>"  onFocus="if (this.value == 'E-mail') { this.value = '' };" onblur="if (this.value == '') { this.value = 'E-mail' };"/><?php if(!empty($msg_erro_email)){echo "<span class='erro'>".nl2br($msg_erro_email)."</span>";}?>

 <select name="sexo" id="sexo" class="input_contato" onchange="">
	<option value="0">Sexo</option>
    <?php foreach($sexosiglas as $sexo02 => $sex){?> 
	<option value="<?php echo $sex; ?>"  <?php echo ($sex==$sexo) ?'selected="selected"':''; ?>><?php echo $sexos[$sexo02]; ?></option>
    <?php } ?>   
	
</select>
<input type="text" name="data_nascimento" class="input_contato" id="nascimento"  maxlength="10" value="<?php echo htmlentities($data_nascimento) ?>"  onFocus="if (this.value == 'Nascimento') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Nascimento' };"/><?php if(!empty($msg_erro_nascimento)){echo "<span class='erro'>".nl2br($msg_erro_nascimento)."</span>";}?>
 <input type="text" name="telefone_t" id="telefone" class="input_contato"  maxlength="15" value="<?php echo htmlentities($telefone_t) ?>"  onFocus="if (this.value == 'Telefone') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Telefone' };"/><?php if(!empty($msg_erro_telefone)){echo "<span class='erro'>".nl2br($msg_erro_telefone)."</span>";}?>
 
  <input type="text" name="telefone_t2" id="telefone2" class="input_contato"  maxlength="15" value="<?php echo htmlentities($telefone_t2) ?>"  onFocus="if (this.value == 'Telefone 2') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Telefone 2' };"/><?php if(!empty($msg_erro_telefone2)){echo "<span class='erro'>".nl2br($msg_erro_telefone2)."</span>";}?>
 
 <?php
  
  $sql = "SELECT nome,id FROM cargos";
	$cargos = $db->GetAll($sql);
    ?>
<select name="cargo" class="input_contato" onchange="">
	<option value="0">Cargo</option>
    <?php foreach($cargos as $car){?> 
	<option value="<?php echo $car['id']; ?>"  <?php echo ($car['id']==$cargo) ?'selected="selected"':''; ?>><?php echo $car['nome']; ?></option>
    <?php } ?>   
	
</select> 
 
 
  
 

<?php
  
  $sql = "SELECT nome,id FROM estado";
	$estados = $db->GetAll($sql);
    ?>
<select name="estado" id="estado" class="input_contato">
    <option value="0">Selecione o estado</option>
  <?php foreach($estados as $estado){?> 
<option value="<?php echo $estado['id']; ?>"  <?php echo ($estado['id']==$estadoescolhido) ?'selected="selected"':''; ?>><?php echo $estado['nome']; ?></option>
<?php } ?>
  
  </select>
  

                            
                            

<select name="cidade" id="cidade" class="input_contato">  
<?php if(!empty($cidade) && $cidade != "Cidade"){
$sql = "SELECT nome,id FROM cidade where estado= $estadoescolhido";
	$cidades = $db->GetAll($sql);
?>
	<?php foreach($cidades as $cid){ ?>
	<option value="<?php echo $cid['id'] ?>" <?php echo ($cid["id"]==$cidade) ?'selected="selected"':'';?>><?php echo $cid['nome']; ?></option>
	<?php }	?>
<?php } ?>

  </select>

<script>
(function($) {
        $('#estado').cascade({
                source: "chama-subcategorias.php",
                cascaded: "cidade",
                extraParams: { ufid: function(){ return $('#estado').val();  } },
                dependentLoadingLabel: "Carregando cidades ...",
                dependentNothingFoundLabel: "Não existem cidades",
                dependentStartingLabel: "Selecione a cidade",
        });     
})(jQuery);
</script>
                    

  
  
  
  
<input type="file" name="curriculo" id="file-2" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
<label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Anexe seu currículo</span></label>
<input type="hidden" name="ano" id="ano" value="<?php echo date("Y");?>"/>
<div id="div_row_error" class="alert alert-danger" role="alert"><strong>Opa!</strong> <span id="div_alert_span_error"></span></div>
<div id="div_row_success" class="alert alert-success" role="alert"><strong>Muito bem!</strong> <span id="div_alert_span_success"></span></div>

<div class="bt_enviar"><input type="submit" border="0" name="submit" value="Enviar" style="outline:none;" /></div>
</form>
</div>
</section>
<script>
      
            $(document).ready(function () {
                $('#div_row_success').hide();
                $('#div_row_error').hide();
                max_size_file = 500000; // => MAX File size
                // => Every time you change file on form
                $("input:file").change(function () {
                    var fileInput = $(this); // => Gets the file data
                    if (fileInput.get(0).files.length) { // => Check if there is a file
                        var fileSize = fileInput.get(0).files[0].size; // => Value in bytes
						var extPermitidas = ['doc','docx', 'pdf', 'DOC', 'DOCX'];
                        var extArquivo = fileInput.get(0).value.split('.').pop();
                        if (fileSize > max_size_file) { // => Check if the file size is bigger tham MAX
                            alertErrorShow('Seu currículo tem peso maior que 2MB. Tente diminuir o peso dele e enviar de novo.');
							document.getElementById("file-2").value = "";
                            return false; // => Ends action
                        }else if (typeof extPermitidas.find(function(ext){ return extArquivo == ext; }) == 'undefined') { // => Check if the file size is bigger tham MAX
                            alertErrorShow('Só são permitidos arquivos .doc, .docx ou pdf.');
							document.getElementById("file-2").value = "";
                            return false; // => Ends action
                        } else {
                            alertSuccessShow('Seu currículo tem o tamanho certo');
                            return false; // => Ends action
                        }
                    } else {
                        alertErrorShow('Você precisa escolher outro arquivo.');
                        return false; // => Ends action
                    }
                });
 
                // => Execute on click submit buttom
                $("#submit_button").click(function () {
                    var fileInput = $('#file_input'); // => Gets the file data
                    if (fileInput.get(0).files.length) { // => Check if there is a file
                        var filename = fileInput.get(0).files[0].name; // => Gets the file name
                        $("#list_group_span_name").html(filename);
                        $("#list_group_span_size").html(fileInput.get(0).files[0].size + ' bytes');
                        $("#list_group_span_type").html(fileInput.get(0).files[0].type);
                        $("#list_group_span_ext").html(filename.split('.').pop());
                        $("#list_group_span_modified").html(fileInput.get(0).files[0].lastModifiedDate);
                        return false; // => This "return false" is just becase i dont want to send de request
                    } else {
                        alertErrorShow('You have to choose one file');
                        return false; // => Ends action
                    }
                });
            });
 
            // => Just show alerts
            function alertErrorShow(message) {
                $('#div_row_success').hide();
                $('#div_alert_span_error').html(message);
                $('#div_row_error').show();
            }
 
            // => Just show alerts
            function alertSuccessShow(message) {
                $('#div_row_error').hide();
                $('#div_alert_span_success').html(message);
                $('#div_row_success').show();
            }
       
                </script>	