<div id="gallotti_aempresa">
<div class="container">
<div class="row row-bottom-padded-lg">
<div class="col-aempresa">
<figure class="animate-box"><img src="imgs/foto_aempresa.jpg" alt="Galloti" class="img-responsive"></figure>
<p class="gallotti_sub_tit animate-box">
No início éramos "Flora Fértil", uma das mais importantes empresas no segmento paisagismo e jardinagem em Sergipe, posteriormente ampliamos também as atividades de comércio varejista de plantas ornamentais e manutenção de áreas verdes. Estudando o mercado local e empresas com expertise em áreas mais abrangentes percebemos que havia carência na área empreendedorística. 
<br><br>
Diante dessa acertiva, criamos a Gallotti Empreendimentos, uma empresa que atua em múltiplos segmentos primando ainda mais pela excelência na prestação de seus serviços. 
<br><br>
Ao longo de três décadas, nos tornamos um grupo com atuação em diversas áreas, consolidando uma carteira de clientes e parceiros que incluem algumas das principais empresas do polo industrial sergipano, além da Prefeitura de aracaju, entre inúmeros outros Municípios, Governo do Estado de Sergipe, Vale e Petrobras. 
<br><br>
Tudo isso fez com que nos tornássemos um grupo sólido, uma holding versátil que trabalha com excelência e cresce junto aos nossos colaboradores, parceiros e clientes. Da Locação de Veículos à prestação dos mais variados serviços, buscamos a cada dia, incorporar novas tecnologias visando sempre o aperfeiçoamento dos nossos negócios.<br><br>
</p>
<h3 class="gallotti_subtitulo_interna animate-box">Missão</h3>
<p class="gallotti_sub_tit animate-box">
Oferecer aos nossos clientes serviços com qualidade, pontualidade e confiabilidade, promovendo o desenvolvimento de nossos colaboradores, visando atingir metas e superar expectativas, contribuindo para o desenvolvimento econômico, social e ambiental.
</p>
<h3 class="gallotti_subtitulo_interna animate-box">Visão</h3>
<p class="gallotti_sub_tit animate-box">
Ser uma empresa inovadora e líder no mercado brasileiro, tornando-se cada vez mais competitiva, buscando a cada dia incorporar uso de novas tecnologias em benefício e melhoramento dos serviços prestados, assumido responsabilidades e comprometimento ético necessários para superarmos as expectativas dos nossos clientes.
</p>
</div>
</div>
<div class="row">
<div id="galeria" class="thumbs">
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/01.jpg" title="A Empresa"><img src="imgs/fotos/01.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/02.jpg" title="A Empresa"><img src="imgs/fotos/02.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/03.jpg" title="A Empresa"><img src="imgs/fotos/03.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/04.jpg" title="A Empresa"><img src="imgs/fotos/04.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/05.jpg" title="A Empresa"><img src="imgs/fotos/05.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/06.jpg" title="A Empresa"><img src="imgs/fotos/06.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/07.jpg" title="A Empresa"><img src="imgs/fotos/07.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/08.jpg" title="A Empresa"><img src="imgs/fotos/08.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<figure class="animate-box"><a class="vlightbox" href="imgs/fotos/09.jpg" title="A Empresa"><img src="imgs/fotos/09.jpg" alt="Locação de Veículos Extrapesados" class="img-responsive"></a></figure>
</div>
</div>
</div>
</div>
</div>
</div>