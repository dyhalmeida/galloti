<section id="gallotti_clientes">
<div class="container">
<div class="row text-center row-bottom-padded-md">
<div class="col-md-8 col-md-offset-2">
<figure class="gallotti_certificado animate-box"><img src="imgs/abnt.jpg" alt="Locação de Veículos Pesados" class="img-responsive"></figure>
<p class="gallotti_sub_tit animate-box">Transporte de resíduos perigosos e locação com e sem motorista de veículos leves, pesados, extrapesados e de passageiros, envolvendo a sede em Aracaju e a filial de Carmópolis</p>
</div>
</div>
<div class="row">
<h2 class="gallotti_tit animate-box fadeInUp animated text-center marginbottom">Clientes</h2>
<div class="col-md-4 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_cliente">
<div class="gallotti_logo_cliente">
<img src="imgs/clientes/aracaju.png" alt="Locação de Veículos Pesados"  height="182" />
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_cliente">
<div class="gallotti_logo_cliente">
<img src="imgs/clientes/cosil.png" alt="Locação de Veículos Pesados"  height="182" />
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_cliente">
<div class="gallotti_logo_cliente">
<img src="imgs/clientes/deso.png" alt="Locação de Veículos Pesados"  height="182" />
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_cliente">
<div class="gallotti_logo_cliente">
<img src="imgs/clientes/habitacional.png" alt="Locação de Veículos Pesados"  height="182" />
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_cliente">
<div class="gallotti_logo_cliente">
<img src="imgs/clientes/petrobras.png" alt="Locação de Veículos Pesados"   height="182" />
</div>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_cliente">
<div class="gallotti_logo_cliente">
<img src="imgs/clientes/vale.png" alt="Locação de Veículos Pesados"   height="182" />
</div>
</div>
</div>
<div class="clearfix visible-sm-block"></div>
</div>
<div class="gallotti_center_position"><p class="animate-box"><a href="clientes.php" target="_blank" class="btn btn-primary">Conheça Todos</a></p></div>
</div>
</section>	