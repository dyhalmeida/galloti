<footer id="gallotti_rodape">
<div class="container">
<div class="row row-bottom-padded-md">
<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_rodape_cln">
<ul class="gallotti_rodape_links">
<li><a href="/./">Home</a></li>
<li><a href="/a-empresa">A Empresa</a></li>
<li><a href="/servicos">Serviços</a></li>
<li><a href="/localizacao">Localização</a></li>
<li><a href="/clientes">Clientes</a></li>
<li><a href="/contato">Contato</a></li>
<li><a href="/trabalhe-conosco">Trabalhe Conosco</a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
<div class="gallotti_rodape_cln">
<p>Gallotti Empreendimentos e Serviços<br>Av. Dr. Edézio Vieira de Melo, 954<br>Bairro Suíssa. Aracaju - Sergipe<br><a href="tel:+7932119778">(79) 3211-9778</a> | <a href="tel:+7932112168">(79) 3211-2168</a><br><a href="mailto:gallotti@gallotti.net.br">gallotti@gallotti.net.br</a></p>
</div>
</div>
<!--<div class="col-md-3 col-sm-6 col-xs-12 animate-box right">
<div class="gallotti_rodape_cln">
<h3>Siga-nos</h3>
<ul class="redes_sociais">
<li><a href="#"><i class="icon-twitter"></i></a></li>
<li><a href="#"><i class="icon-facebook"></i></a></li>
<li><a href="#"><i class="icon-google-plus"></i></a></li>
<li><a href="#"><i class="icon-instagram"></i></a></li>
<li><a href="#"><i class="icon-youtube-play"></i></a></li>
</ul>
</div>
</div>-->
</div>
</div>
<div class="gallotti_assinatura animate-box">
<div class="container">
<div class="row">
<div class="col-md-12">
<p class="gallotti_left"><small>&copy; 2016 Gallotti Empreendimentos e Serviços</small></p><p class="gallotti_right"><small class="gallotti_right"><div class="goweb"><a href="https://www.gowebtecnologia.com.br" target="_blank" title="Criação de site"><img src="https://www.gowebtecnologia.com.br/imgs/goweb_retina_claro.png" height="26px" alt="criação de site"></a></div></small></p>
</div>
</div>
</div>
</div>
</footer>	