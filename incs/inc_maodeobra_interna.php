<div id="gallotti_aempresa">
<div class="container">
<div class="row row-bottom-padded-lg">
<div class="col-aempresa">
<h3 class="gallotti_subtitulo_interna animate-box">Terceirização de Mão de Obra</h3>
<p class="gallotti_sub_tit animate-box">
A mão de obra especializada que sua empresa procura.
<br><br>
A Gallotti Empreendimentos tem por objetivo prestar serviços de excelência, oferecendo o suporte que nossos clientes necessitam para executar as suas atividades. Para isso, investimos sempre no aperfeiçoamento e treinamento de nossa equipe de colaboradores.
<br><br>
</p>
<h4 class="gallotti_subtitulo_interna animate-box">Meio Ambiente</h4>
<p class="gallotti_sub_tit animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Manutenção de áreas verdes comerciais, residenciais e industriais<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Serviços de jardinagem, inclusive poda de árvores<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Plantio de mudas ornamentais e tratamento fitossanitário<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Elaboração e execução de Projetos de Paisagismo<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Serviços de reflorestamento<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Supressão de vegetação<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Consultoria em questões ambientais e de sustentabilidade<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Serviços de limpeza de praias<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Varrição, roçagem e capinação<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Implantação de sistemas de irrigação
</p>
<h4 class="gallotti_subtitulo_interna animate-box">Locação de Mão de Obra</h4>
<p class="gallotti_sub_tit animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Limpeza e manutenção predial<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Limpeza e manutenção industrial<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Limpeza urbana<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Limpeza de fossas sépticas e caixas de gordura<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Limpeza de solo contaminado<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Limpeza de tanques de infiltração<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Medição de consumo de energia elétrica, gás e água<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Serviços telefônicos de recepção e chamadas com operador humano<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Serviços administrativos<br>
<img src="imgs/mc.png" width="15" height="9" alt="Terceirização de Mão de Obra" />Operação de equipamentos e digitação de dados para terceiros
</p>
</div>
</div>
</div>
</div>