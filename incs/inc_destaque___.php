<?php
error_reporting(0);
ini_set("display_errors", 0 );
$bgs = array(
'<section id="gallotti_main" class="js-fullheight" style="background-image: url(imgs/extra-pesados.jpg);" data-next="yes">
<div class="gallotti_overlay"></div>
<div class="container">
<div class="gallotti_destaque js-fullheight">
<div class="gallotti_destaque_texto">
<div class="gallotti_left_position">
<h2 class="animate-box">Veículos Extrapesados<br /><div class="descricao_destaque">Executamos transporte rodoviário de cargas pesadas, equipamentos, produtos industriais líquidos e sólídos. </div></h2>
<p class="animate-box"><a href="transporte.php" class="btn btn-primary">Saiba Mais</a></p>
</div>
</div>
</div>
</div>
<div class="gallotti_leia_mais animate-box">
<a href="#" class="scroll-btn">
<span class="text">Conheça-nos mais</span>
<span class="arrow"><i class="icon-chevron-down"></i></span>
</a>
</div>
</section>',
'<section id="gallotti_main" class="js-fullheight" style="background-image: url(imgs/passageiros.jpg);" data-next="yes">
<div class="gallotti_overlay"></div>
<div class="container">
<div class="gallotti_destaque js-fullheight">
<div class="gallotti_destaque_texto">
<div class="gallotti_left_position">
<h2 class="animate-box">Transporte de Passageiros<br /><div class="descricao_destaque">Temos o veículo ideal para as necessidades da sua empresa</div></h2>
<p class="animate-box"><a href="transporte.php" class="btn btn-primary">Saiba Mais</a></p>
</div>
</div>
</div>
</div>
<div class="gallotti_leia_mais animate-box">
<a href="#" class="scroll-btn">
<span class="text">Conheça-nos mais</span>
<span class="arrow"><i class="icon-chevron-down"></i></span>
</a>
</div>
</section>',
'<section id="gallotti_main" class="js-fullheight" style="background-image: url(imgs/maodeobra.jpg);" data-next="yes">
<div class="gallotti_overlay"></div>
<div class="container">
<div class="gallotti_destaque js-fullheight">
<div class="gallotti_destaque_texto">
<div class="gallotti_left_position">
<h2 class="animate-box">Locação de Mão de Obra<br /><div class="descricao_destaque">A mão de obra especializada que a sua empresa procura</div></h2>
<p class="animate-box"><a href="terceirizacao-de-mao-de-obra.php" class="btn btn-primary">Saiba Mais</a></p>
</div>
</div>
</div>
</div>
<div class="gallotti_leia_mais animate-box">
<a href="#" class="scroll-btn">
<span class="text">Conheça-nos mais</span>
<span class="arrow"><i class="icon-chevron-down"></i></span>
</a>
</div>
</section>',
'<section id="gallotti_main" class="js-fullheight" style="background-image: url(imgs/construcao.jpg);" data-next="yes">
<div class="gallotti_overlay"></div>
<div class="container">
<div class="gallotti_destaque js-fullheight">
<div class="gallotti_destaque_texto">
<div class="gallotti_left_position">
<h2 class="animate-box">Construção e Administração de Obra<br /><div class="descricao_destaque">Na hora de contruir ou reformar, conte com os serviços da Gallotti Empreendimentos</div></h2>
<p class="animate-box"><a href="construcao-e-administracao-de-obra.php" class="btn btn-primary">Saiba Mais</a></p>
</div>
</div>
</div>
</div>
<div class="gallotti_leia_mais animate-box">
<a href="#" class="scroll-btn">
<span class="text">Conheça-nos mais</span>
<span class="arrow"><i class="icon-chevron-down"></i></span>
</a>
</div>
</section>',
);

// randômico
$random_no = count($bgs);
$random = $random_no-1;
mt_srand ((double) microtime () * 1000000);
$rnd = mt_rand(0,$random);

// visualização
$bg=$bgs[$rnd];
?>
<?php echo $bg ; ?>
		