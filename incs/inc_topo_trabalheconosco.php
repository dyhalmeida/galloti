<section id="gallotti_main" class="no-js-fullheight" style="background-image: url(imgs/foto_trabalheconosco_principal.jpg);" data-next="yes">
<div class="gallotti_overlay"></div>
<div class="container">
<div class="gallotti_destaque no-js-fullheight">
<div class="gallotti_destaque_texto">
<div class="gallotti_center_position">
<h2 class="animate-box">Trabalhe Conosco</h2>
<h3 class="animate-box">Quer trabalhar na Gallotti ou na Flora Fértil? Então preencha o formulário abaixo e anexe um currículo.</h3>
</div>
</div>
</div>
</div>
<div class="gallotti_leia_mais animate-box"><a href="#" class="scroll-btn"><span class="arrow"><i class="icon-chevron-down"></i></span></a></div>
</section>