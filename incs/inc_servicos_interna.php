<div id="gallotti_aempresa">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<a href="transporte.php">
<figure class="animate-box"><img src="imgs/serv_locacaodeveiculos.jpg" alt="Locação de Veículos" class="img-responsive"></figure>
<h3 class="gallotti_subtitulo_interna animate-box">Transporte</h3>
</a>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<a href="terceirizacao-de-mao-de-obra.php">
<figure class="animate-box"><img src="imgs/serv_limpezaeconservacaopredial.jpg" alt="Terceirização de mão de obra" class="img-responsive"></figure>
<h3 class="gallotti_subtitulo_interna animate-box">Terceirização de Mão de Obra</h3>
</a>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<a href="construcao-e-administracao-de-obra.php">
<figure class="animate-box"><img src="imgs/serv_construcaocivil.jpg" alt="Construção e Administração de Obra" class="img-responsive"></figure>
<h3 class="gallotti_subtitulo_interna animate-box">Construção e Administração de Obra</h3>
</a>
</div>
</div>
</div>
</div>
</div>       