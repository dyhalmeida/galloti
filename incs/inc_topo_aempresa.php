<section id="gallotti_main" class="no-js-fullheight" style="background-image: url(imgs/foto_aempresa_principal.jpg);" data-next="yes">
<div class="gallotti_overlay"></div>
<div class="container">
<div class="gallotti_destaque no-js-fullheight">
<div class="gallotti_destaque_texto">
<div class="gallotti_center_position">
<h2 class="animate-box">A Empresa</h2>
<h3 class="animate-box">Conheça a história da Gallotti Empreendimentos e Serviços e veja por que ela tem a combinação perfeita entre eficiência e qualidade.</h3>
</div>
</div>
</div>
</div>
<div class="gallotti_leia_mais animate-box">
<a href="#" class="scroll-btn"><span class="arrow"><i class="icon-chevron-down"></i></span></a>
</div>
</section>