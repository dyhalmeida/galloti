<section id="gallotti_contato">
<div class="container">
<div class="col-contato col-sm-6 col-xs-12 animate-box">
<div class="gallotti_rodape-contato">
<p>
Av. Dr. Edézio Vieira de Melo, 954<br>Bairro Suíssa. Aracaju - Sergipe<br><a href="tel:+7932119778">(79) 3211-9778</a> | <a href="tel:+7932112168">(79) 3211-2168</a><br><a href="mailto:gallotti@gallotti.net.br">gallotti@gallotti.net.br</a> 
</p>
</div>
</div>
<form action="submeter.php" method="post" class="animate-box">
<?php if(!empty($errors)){echo "<div class='erro_contato'>".nl2br($errors)."</div>";}?>
<input type="text" name="nome" class="input_contato"  maxlength="80" value="<?php echo htmlentities($nome) ?>"  onFocus="if (this.value == 'Nome') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Nome' };"/>
<input type="text" name="email" class="input_contato"   maxlength="100" value="<?php echo htmlentities($email) ?>"  onFocus="if (this.value == 'E-mail') { this.value = '' };" onblur="if (this.value == '') { this.value = 'E-mail' };"/>
<input type="text" name="telefone" id="telefone" class="input_contato"  maxlength="15" value="<?php echo htmlentities($telefone) ?>"  onFocus="if (this.value == 'Telefone') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Telefone' };"/>
<input type="hidden" name="ano" id="ano" value="<?php echo date("Y");?>"/>
<textarea name="mensagem" class="text_contato" onFocus="if (this.value == 'Mensagem') { this.value = '' };" onblur="if (this.value == '') { this.value = 'Mensagem' };"><?php echo htmlentities($mensagem) ?></textarea>
<div class="bt_enviar"><input type="submit" border="0" name="submit" value="Enviar" style="outline:none;" /></div>
</form>
</div>
</section>