<section id="gallotti_conteudo">
<div class="container">
<div class="row row-bottom-padded-md">
<div class="col-md-6 col-md-offset-3 text-center">
<h2 class="gallotti_tit animate-box">Serviços</h2>
<p class="gallotti_sub_tit animate-box">Os serviços da Gallotti possuem a combinação perfeita entre eficiência e qualidade</p>
</div>
</div>
<div class="row">
<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
<a href="#" class="gallotti_servico_home image-popup">
<img src="imgs/serv_locacaodeveiculos.jpg" alt="Localção de Veículos" class="img-responsive">
<div class="gallotti_textos">
<h2>Locação de Veículos</h2>
<p>A Gallotti é especializada em locação de veículos leves, pesados e extrapesados</p>
</div>
</a>
</div>
<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
<a href="#" class="gallotti_servico_home image-popup">
<img src="imgs/serv_transportedeaguapotavel.jpg" alt="Localção de Veículos Pesados" class="img-responsive">
<div class="gallotti_textos">
<h2>Transporte de Água Potável</h2>
<p>Fazemos o transporte de água potável através de carros pipa</p>
</div>
</a>
</div>
<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
<a href="#" class="gallotti_servico_home image-popup">
<img src="imgs/serv_succaoavacuo.jpg" alt="Localção de Veículos Extrapesados" class="img-responsive">
<div class="gallotti_textos">
<h2>Sucção a Vácuo</h2>
<p>A Gallotti faz a coleta de fossas e caixas de gordura através de sucção a vácuo</p>
</div>
</a>
</div>
<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
<a href="#" class="gallotti_servico_home image-popup">
<img src="imgs/serv_limpezaeconservacaopredial.jpg" alt="Terceirização de mão de obra" class="img-responsive">
<div class="gallotti_textos">
<h2>Limpeza e Conservação Predial</h2>
<p>A limpeza de um Condomínio deve ser executada por profissionais qualificados</p>
</div>
</a>
</div>
<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
<a href="#" class="gallotti_servico_home image-popup">
<img src="imgs/serv_construcaocivil.jpg" alt="Administração de Contrução de Obras" class="img-responsive">
<div class="gallotti_textos">
<h2>Construção Civil</h2>
<p>Edificações (residenciais, comerciais e industriais)</p>
</div>
</a>
</div>
<div class="col-md-4 col-sm-6 col-xxs-12 animate-box">
<a href="imgs/serv_administracaodeobra.jpg" class="gallotti_servico_home image-popup">
<img src="imgs/serv_administracaodeobra.jpg" alt="Locação de Veículos Pesados" class="img-responsive">
<div class="gallotti_textos">
<h2>Administração de Obras</h2>
<p>Na hora de construir ou reformar conte com os serviços da Gallotti Empreendimentos</p>
</div>
</a>
</div>
</div>
<div class="gallotti_center_position"><p class="animate-box"><a href="servicos.php" target="_blank" class="btn btn-primary">Saiba Mais</a></p></div>
</div>
</section>