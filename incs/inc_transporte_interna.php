<div id="gallotti_aempresa">
<div class="container">
<div class="row row-bottom-padded-lg">
<div class="col-aempresa">
<h3 class="gallotti_subtitulo_interna animate-box">Transporte</h3>
<p class="gallotti_sub_tit animate-box">
Nosso Catálogo Contempla todas as categorias para atender as suas necessidades. Desde 2003, iniciamos a locação de caminhões para Empresa Municipal de Serviços Urbanos - EMSURB, locamos posteriormente Caminhões Tanque (Pipa) para a Companhia de Saneamento de Sergipe - DESO, atendendo todo Estado de Sergipe, além de <strong>Locação de Veículos</strong> Leves (Executivos, Utilitários, Passeio e de Transporte de Passageiros) para diversas empresas e caminhões de Sucção a Vácuo (Limpa fossa), destinados a coleta e transporte de resíduos perigosos como efluentes sanitários e caixas de gordura. 
<br><br>
Mais recentemente, em meados de 2012 passamos a trabalhar com a categoria de <strong>Veículos Extra Pesados</strong> (Carretas), mediante contrato com uma das principais empresas do País, (Petrobras S/A), executando o Transporte Rodoviário de cargas pesadas e de equipamentos, produtos industriais líquidos e sólidos nos Estados de Sergipe e Alagoas.
<br><br>
</p>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">	
<h4 class="gallotti_subtitulo_interna animate-box" style="float:left; width:100%; text-align:left;">VEÍCULOS LEVES</h4>
<p class="gallotti_servicos_lista animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Leves" />Passeio<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Leves" />Utilitário<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Leves" />Executivo<br>
</p>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<h4 class="gallotti_subtitulo_interna animate-box" style="float:left; width:100%; text-align:left;">VEÍCULOS PESADOS</h4>
<p class="gallotti_servicos_lista animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Pesados" />Caminhão Carroceria Fechada (Baú)<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Pesados" />Caminhão Basculantes<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Pesados" />Caminhão Carroceria Aberta<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Pesados" />Caminhão Tanque Inox (Pipa)<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Pesados" />Caminhão Tanque Sucção a Vácuo (Limpa Fossa)
</p>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<h4 class="gallotti_subtitulo_interna animate-box" style="float:left; width:100%; text-align:left;">VEÍCULOS EXTRAPESADOS</h4>
<p class="gallotti_servicos_lista animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Extrapesados" />Cavalo Mecânico (4x2, 6x2 e 6x4)<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos Extrapesados" />Implementos Rodoviários:<br>
- Carreta Tanque Inox<br>
- Carreta Carrega Tudo<br>
- Carreta Carga Seca<br>
- Carreta Graneleira<br>
- Carreta Baú Simples, Sider e Frigorífico
</p>
</div>
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
<div class="gallotti_galeria">
<h4 class="gallotti_subtitulo_interna animate-box" style="float:left; width:100%; text-align:left;">VEÍCULOS PARA PASSAGEIROS</h4>
<p class="gallotti_servicos_lista animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos para passageiros" />Vans<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos para passageiros" />Micro-ônibus<br>
<img src="imgs/mc.png" width="15" height="9" alt="Locação Veiculos para passageiros" />Ônibus
</p>
</div>
</div>
</div>
</div>
</div>
</div>