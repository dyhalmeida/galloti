<div id="gallotti_aempresa">
<div class="container">
<div class="row row-bottom-padded-lg">
<div class="col-aempresa">
<h3 class="gallotti_subtitulo_interna animate-box">Construção e Administração de Obra</h3>
<p class="gallotti_sub_tit animate-box">
Na hora de construir ou reformar conte com os serviços da Gallotti Empreendimentos.
<br><br>
Dentro da nossa política de ampliação dos serviços, iniciamos em 2013 a atuação no segmento da construção civil, construindo e reformando praças, residências, conjunto de casas populares, quadras poliesportivas, entre outras. 
<br><br></p>
<h4 class="gallotti_subtitulo_interna animate-box">Serviços Técnicos</h4>
<p class="gallotti_sub_tit animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de serralheria/caldeiraria<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de Pintura<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de elaboração de projetos Técnicos de Arquitetura e Engenharia<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de elaboração, consultoria e assessoria em projetos de Engenharia Ambiental<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de instalação e manutenção elétricao<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de instalação hidráulicas, sanitárias e de gás<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Serviços de instalação de sistemas centrais de ar condicionado<br>
</p>
<h4 class="gallotti_subtitulo_interna animate-box">Construção</h4>
<p class="gallotti_sub_tit animate-box">
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Edificações residenciais, comerciais e industriais<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Urbanização: ruas, praças e calçadas<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Desmonte e demolição de estruturas previamente existentes<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Estações, redes de distribuição de energia elétrica, redes de abastecimento de água, coleta de esgoto e construções correlatas<br>
<img src="imgs/mc.png" width="15" height="9" alt="Construção e Administração de Obraa" />Obras de terraplenagem<br>
</p>
</div>
</div>
</div>
</div>