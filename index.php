<?php
if(strpos($_SERVER['REQUEST_URI'], 'index.php') !== false){
	$url02 = '/';
	header('location: ' . $url02);exit;
}
?>
<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Gallotti Empreendimentos e Serviços | Locação de Veículos | Locação de Mão de Obra</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="A Gallotti tem serviços como Locação de Veículos, Locação de veículos leves, Locação de Mão de Obra, Locação de Veículos Pesados, limpeza e conservação predial, transporte de cargas rodoviárias, construção civil, entre outros." />
<meta name = "Author" content = "GOWEB Tecnologia - http://www.gowebtecnologia.com.br">
<meta name="publisher" content="GOWEB Tecnologia - http://www.gowebtecnologia.com.br" />
<meta name="robots" content="index, follow">
<link rel="shortcut icon" href="favicon.ico">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,600,400italic,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/animate.css"><link rel="stylesheet" href="css/flexslider.css"><link rel="stylesheet" href="css/icomoon.css"><link rel="stylesheet" href="css/magnific-popup.css"><link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/gallotti.css">
<script src="js/modernizr-2.6.2.min.js"></script>
<!--[if lt IE 9]><script src="js/respond.min.js"></script><![endif]-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script type='text/javascript'>//<![CDATA[
$(function(){
$(document).ready(function () {
    $('#openBtn').ready(function () {
        $('#myModal').modal()
    });

    $('.modal')
        .on({
            'show.bs.modal': function() {
                var idx = $('.modal:visible').length;
                $(this).css('z-index', 1040 + (10 * idx));
            },
            'shown.bs.modal': function() {
                var idx = ($('.modal:visible').length) - 1; // raise backdrop after animation.
                $('.modal-backdrop').not('.stacked')
                .css('z-index', 1039 + (10 * idx))
                .addClass('stacked');
            },
            'hidden.bs.modal': function() {
                if ($('.modal:visible').length > 0) {
                    // restore the modal-open class to the body element, so that scrolling works
                    // properly after de-stacking a modal.
                    setTimeout(function() {
                        $(document.body).addClass('modal-open');
                    }, 0);
                }
            }
        });
});
});//]]> 

</script>

</head>
<!--  onLoad="openBtn" -->
<body>
<!--<div class="modal fade" id="myModal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>  </div>
<div class="modal-body"><img src="imgs/banner-sipat.jpg" width="760"><br><br><a data-toggle="modal" href="#myModal2" class="btn btn-primary" style="color:#FFF;">Clique aqui e confira a programação</a></div></div>
</div>
</div>
<div class="modal fade" id="myModal2"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></div>
<div class="container"></div> <div class="modal-body"><img src="imgs/banner-sipat2.jpg" width="760"></div></div></div></div>
-->

<?php include 'incs/inc_google.php'; ?>
<div class="gallotti_carregando"></div>
<div id="gallotti_page">
<?php include 'incs/inc_menu.php'; ?>
<?php include 'incs/inc_destaque.php'; ?>
<?php include 'incs/inc_servicos.php'; ?>
<?php include 'incs/inc_clientes.php'; ?>
<?php include 'incs/inc_rodape.php'; ?>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>