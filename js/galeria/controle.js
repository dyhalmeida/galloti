			jQuery(function( $ ) {

				//	Add a custom filter to recognize images from lorempixel (that don't end with ".jpg" or something similar)
				$.tosrus.defaults.media.image = {
					filterAnchors: function( $anchor ) {
						return $anchor.attr( 'href' ).indexOf( 'imgs/' ) > -1;
					}
				};

				$('#galeria a').tosrus({
					buttons: 'inline',
					pagination	: {
						add			: true,
						type		: 'thumbnails'
					}
				});

	
			});